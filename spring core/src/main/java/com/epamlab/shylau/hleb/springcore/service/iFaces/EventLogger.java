package com.epamlab.shylau.hleb.springcore.service.iFaces;

public interface EventLogger {
    void logEvent(String msg);
}
