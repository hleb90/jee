package com.epamlab.shylau.hleb.springcore.service;

import com.epamlab.shylau.hleb.springcore.service.iFaces.EventLogger;

public class ConsoleEventLogger implements EventLogger {

    public void logEvent(String msg) {
        System.out.println(msg);
    }
}
